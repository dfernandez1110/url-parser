# URL Parser Exercise

This exercise involves creating a logic that extracts the variable parts of a URL into a dictionary. The keys of the dictionary will be the "names" of the variable parts of a URL, and the values of the dictionary will be the values.

## Requirements

We will be supplied with:

1. A URL format string, which describes the format of a URL. A URL format string can contain constant parts and variable parts, in any order, where "parts" of a URL are separated with "/". All variable parts begin with a colon. Here is an example of such a URL format string: '/:version/api/:collection/:id'

2. A particular URL instance that is guaranteed to have the format given by the URL format string. It may also contain URL parameters. For example, given the example URL format string above, the URL instance might be: '/6/api/listings/3?sort=desc&limit=10'

Given this example URL format string and URL instance, the dictionary we want that maps all the variable parts of the URL instance to their values would look like this:

```python
{
'version': 6,
'collection': 'listings',
'id': 3,
'sort': 'desc',
'limit': 10
}
```

# Run url parser on console
### Usage:
```bash 
   python run_parser.py <url_format> <url_instance>
```
### Example: 
```bash
python run_parser.py "/:version/api/:collection/:id" "/6/api/listings/3?sort=desc&limit=10"
```


