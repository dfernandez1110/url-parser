import sys
sys.path.append('..')
from src.parse_url import url_parser
import pytest

@pytest.mark.parametrize("url_format, url_instance, expected", [
    ('/:version/api/:collection/:id', '/6/api/listings/3?sort=desc&limit=10', {'version': 6, 'collection': 'listings', 'id': 3, 'sort': 'desc', 'limit': 10}),
    ('/:version/api/:collection/:id', '/7/api/users/4?sort=asc&limit=5', {'version': 7, 'collection': 'users', 'id': 4, 'sort': 'asc', 'limit': 5}),
    ('/api/:version/:collection/:id', '/api/7/users/4?sort=asc&limit=5', {'version': 7, 'collection': 'users', 'id': 4, 'sort': 'asc', 'limit': 5})
])  

def test_url_parser(url_format, url_instance, expected):
    assert url_parser(url_format, url_instance) == expected

            