import argparse
from src.parse_url import url_parser

# Usage: python url_parser.py <url_format> <url_instance>
# Example: python url_parser.py "/:version/api/:collection/:id" "/6/api/listings/3?sort=desc&limit=10"

def main():
    parser = argparse.ArgumentParser(description='Parse a URL instance based on a URL format.')
    parser.add_argument('url_format', type=str, help='The URL format string.')
    parser.add_argument('url_instance', type=str, help='The URL instance string.')
    args = parser.parse_args()

    result = url_parser(args.url_format, args.url_instance)
    print(result)

if __name__ == "__main__":
    main()